/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.my.company.softdevlab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lenovo
 */
public class lab3_2UnitTest extends Softdevlab3 {
    
    public lab3_2UnitTest() {
    }
 
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //TDD Test Driven Delopment
    
    
    //Row
    //1
    @Test
    public void testCheckRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //2
    @Test
    public void testCheckRow2_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //3
    @Test
    public void testCheckRow3_O_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //Collumm
    //4
    @Test
    public void testCheckCol1_O_output_true(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //5
    @Test
    public void testCheckCol2_O_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //6
    @Test
    public void testCheckCol3_O_output_true(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //X
    //7
    @Test
    public void testCheckX1_O_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //8
    @Test
    public void testCheckX2_O_output_true(){
        String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //Draw
    //9
    @Test
    public void testCheckDraw_O_output_true(){
        String[][] table = {{"O","X","X"},{"X","O","O"},{"X","O","X"}};
        String currentPlayer = "O";
        String enemy = "X";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    
    
    //Row
    //10
    @Test
    public void testCheckRow1_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //11
    @Test
    public void testCheckRow2_X_output_true(){
        String[][] table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //12
    @Test
    public void testCheckRow3_X_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //Collumm
    //13
    @Test
    public void testCheckCol1_X_output_true(){
        String[][] table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //14
    @Test
    public void testCheckCol2_X_output_true(){
        String[][] table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //15
    @Test
    public void testCheckCol3_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //X1
    //16
    @Test
    public void testCheckX1_X_output_true(){
        String[][] table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //X2
    //17
    @Test
    public void testCheckX2_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
    
    //Draw
    //18
    @Test
    public void testCheckDraw_X_output_true(){
        String[][] table = {{"X","O","O"},{"O","X","X"},{"O","X","O"}};
        String currentPlayer = "X";
        String enemy = "O";
        boolean result = Softdevlab3.checkWin(table,currentPlayer,enemy);      
        assertEquals(true,result);      
    }
}
