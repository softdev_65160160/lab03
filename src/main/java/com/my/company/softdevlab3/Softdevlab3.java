/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.my.company.softdevlab3;

/**
 *
 * @author Lenovo
 */
import java.util.Scanner;
public class Softdevlab3 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'X';
    static int row,col;
    static int round = 0;
    static char continuePlaying;
    
    public static void main(String[] args) {     
              
        while(true){
            printWelcom();
            while(true){            
                printTable();
                printTurn();
                inputRowcol();
                //printTable();
                System.out.println();
                if (isWin()){
                    printTable();
                    printWinner();
                    break;
                }
                if (isDraw()){
                    printTable();
                    printDraw();
                    break;
                }
                switchPlayer();
                round++;
            }
            if(!inputContinue()){
                break;
            }
            
        }
    }
    


    private static void printWelcom() {
       System.out.println("Welcome to XO");
    }

    private static void printTable() {
        for (int i=0;i<table.length;i++){
            for (int j=0;j<table.length;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }    
    }
    
    private static void printTurn() {
        System.out.println(currentPlayer+" Turn");    
    }
    
    
    private static void inputRowcol() {
        while(true){
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input row,col :");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row-1][col-1]=='-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
            System.out.println("This position has already been used!!!. please try again");
        }
    }

    public static boolean isWin() {
        if (checkRow()){
            return true;
        }
        if (checkCol()){
            return true;
        }
        if (checkX1()){
            return true;
        }
        if (checkX2()){
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        if (round == 8){
            return true;
        }
        return false;
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }

    private static void printWinner() {
        System.out.println(currentPlayer+ " is a winner!");
    }

    private static boolean checkRow() {
        for(int i=0;i<3;i++){
            if (table[row-1][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for(int i=0;i<3;i++){
            if (table[i][col-1]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        for(int i=0;i<3;i++){
            if (table[i][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for(int i=0;i<3;i++){
            if (table[i][2-i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Game is a draw!");
    }

    private static boolean inputContinue() {
        while(true){
            Scanner sc = new Scanner(System.in);
            System.out.print(" please input continue(y/n) :");
            continuePlaying = sc.next().toLowerCase().charAt(0);
            if (continuePlaying == 'y'){
                return true;               
            }else if(continuePlaying == 'n'){
                return false;
            }
            System.out.println("Invalid input!!! please try again");
        }      
    }
    
    
    
    
    
    //checker XO auto
    static boolean checkWin(String[][] table,String currentPlayer,String enemy){
        if (checkRow(table, currentPlayer)) {
            return true;           
        }else if (checkCol(table,currentPlayer)){
            return true;
        }else if (checkX1(table,currentPlayer)){
            return true;
        }else if (checkX2(table,currentPlayer)){
            return true;
        }else if (checkDraw(table,currentPlayer,enemy)){
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table,currentPlayer,row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer,int row) {
        for (int i=0;i<3;i++){
            if (table[row][i].equals(currentPlayer)){
                return true;
            }
        }
        return false;
    }
    
    
    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col<3; col++) {
            if (checkCol(table,currentPlayer,col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer,int col) {
        for (int i=0;i<3;i++){
            if (table[i][col].equals(currentPlayer)){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkX1(String[][] table,String currentPlayer) {
        for(int i=0;i<3;i++){
            if (!table[i][i].equals(currentPlayer)){
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(String[][] table,String currentPlayer) {
        for(int i=0;i<3;i++){
            if (!table[i][2-i].equals(currentPlayer)){
                return false;
            }
        }
        return true;
    }
    
    public static boolean checkDraw(String[][] table,String currentPlayer,String enemy) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!table[i][j].equals(currentPlayer) && !table[i][j].equals(enemy)) {
                    return false;
                }
            }
        }
        return true;

    }
    
}
